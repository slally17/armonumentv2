﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pulseScript : MonoBehaviour
{
    [SerializeField] float animTime1 = 0.25f;
    [SerializeField] float animTime2 = 0.25f;
    [SerializeField] float pulseAmount = 1.25f;

    private bool clicked1 = false;
    private bool clicked2 = false;
    private float startTime1 = -1000;
    private float startTime2 = -1000;
    private Vector3 startingScale;

    private void Start()
    {
        startingScale = transform.localScale;
    }

    private void Update()
    {
        if (clicked1)
        {
            float t = (Time.time - startTime1) / animTime1;
            transform.localScale = Vector3.Slerp(startingScale, startingScale * pulseAmount, t);
            if (t > 1)
            {
                clicked1 = false;
                clicked2 = true;
                startTime2 = Time.time;
            }
        }
        else if (clicked2)
        {
            float t = (Time.time - startTime2) / animTime2;
            transform.localScale = Vector3.Slerp(startingScale * pulseAmount, startingScale, t);
            if (t > 1)
            {
                clicked2 = false;
            }
        }
    }

    private void OnMouseDown()
    {
        clicked1 = true;
        startTime1 = Time.time;
    }
}
