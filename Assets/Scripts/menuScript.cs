﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuScript : MonoBehaviour
{
    [SerializeField] private GameObject iPadBackground;
    [SerializeField] private GameObject iPhoneBackground;

    private void Start()
    {
        if (SystemInfo.deviceModel.StartsWith("iPhone"))
        {
            iPadBackground.SetActive(false);
            iPhoneBackground.SetActive(true);

        }
        else if (SystemInfo.deviceModel.StartsWith("iPad"))
        {
            iPadBackground.SetActive(true);
            iPhoneBackground.SetActive(false);
        }
    }

    public void StartButton()
    {
        SceneManager.LoadScene("AR_Scene_MD");
    }
}
