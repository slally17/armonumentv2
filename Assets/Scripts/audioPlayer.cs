﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayer : MonoBehaviour
{
    [SerializeField] private List<AudioClip> sounds;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private audioVisuals audioVisuals;
    [SerializeField] private bool deleteSounds;
    [SerializeField] private bool changeColor = false;
    [SerializeField] private Color newColor;
    [SerializeField] private GameObject garden;

    private void Update()
    {
        if (audioSource.isPlaying && sounds.Count == 0 && changeColor)
        {
            gameObject.GetComponent<Renderer>().material.SetColor("_Color", newColor);         
        }
        else if (!audioSource.isPlaying && sounds.Count == 0)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnMouseDown()
    {
        if(sounds.Count > 0)
        {
            int position = Random.Range(0, sounds.Count);
            audioSource.clip = sounds[position];
            audioSource.Play();

            if(deleteSounds)
            {
                sounds.RemoveAt(position);
            }

            audioVisuals.position = transform.position;
            audioVisuals.garden = garden;
        }
    }
}
