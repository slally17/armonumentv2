﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plantPosition : MonoBehaviour
{
    [SerializeField] private Transform camera;
    [SerializeField] private GameObject plants;

    // Update is called once per frame
    void Update()
    {
        if(plants.activeSelf)
        {
            Vector3 newPosition = new Vector3(camera.position.x, transform.position.y, camera.position.z);
            plants.transform.position = newPosition;
        }
    }
}
