﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioVisuals : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;

    [SerializeField] private GameObject originalStatue;
    public GameObject garden;

    [SerializeField] private GameObject characters;

    [SerializeField] private GameObject cameraObj;

    [SerializeField] private GameObject sphere;
    [SerializeField] private float animTime = 5.0f;
    [HideInInspector] public Vector3 position;
    [HideInInspector] public bool sphereClosed = false;

    private bool playing = false;
    private float startTime;
    private Vector3 startingPosition;
    private Vector3 startingScale;

    private void Start()
    {
        originalStatue.SetActive(true);
        garden.SetActive(false);

        characters.SetActive(true);

        sphere.SetActive(false);

        playing = false;
    }

    void Update()
    {
        if (!playing && audioSource.isPlaying)
        {
            originalStatue.SetActive(false);
            garden.SetActive(true);

            characters.SetActive(false);

            sphere.SetActive(true);
            sphere.transform.position = position;

            playing = true;
            startTime = Time.time;

            startingPosition = sphere.transform.position;
            startingScale = sphere.transform.localScale;
        }
        else if (playing && !audioSource.isPlaying && false)
        {
            originalStatue.SetActive(true);
            garden.SetActive(false);

            characters.SetActive(true);

            playing = false;
            startTime = Time.time;

            startingPosition = sphere.transform.position;
            startingScale = sphere.transform.localScale;
        }
        else if (playing)
        {
            float t = (Time.time - startTime) / animTime;
            sphere.transform.position = Vector3.Slerp(startingPosition, cameraObj.transform.position, t);
            sphere.transform.localScale = Vector3.Slerp(startingScale, new Vector3(80, 80, 80), t);
        }
        else if (!playing)
        {
            float t = (Time.time - startTime) / animTime;
            sphere.transform.position = Vector3.Slerp(startingPosition, position, t);
            sphere.transform.localScale = Vector3.Slerp(startingScale, new Vector3(5, 5, 5), t);
            if (t > 1)
            {
                sphere.SetActive(false);
            }
        }

        if(sphereClosed)
        {
            closeAudioSphere();
            sphereClosed = false;
        }
    }

    private void closeAudioSphere()
    {
        audioSource.Stop();

        originalStatue.SetActive(true);
        garden.SetActive(false);

        characters.SetActive(true);
        
        playing = false;
        startTime = Time.time;

        startingPosition = sphere.transform.position;
        startingScale = sphere.transform.localScale;
    }
}
