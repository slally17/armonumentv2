﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class characterMove : MonoBehaviour
{
    [SerializeField] private GameObject[] path;
    [SerializeField] private float minDist;

    private NavMeshAgent agent;
    private Vector3 distance;
    private int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        agent.destination = path[count].transform.position;
        distance = path[count].transform.position - transform.position;

        if(distance.magnitude < minDist)
        {
            count++;
            if(count == path.Length)
            {
                count = 0;
            }
        }
    }
}
