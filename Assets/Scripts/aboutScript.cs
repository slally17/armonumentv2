﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aboutScript : MonoBehaviour
{
    [SerializeField] private GameObject aboutButton;
    [SerializeField] private GameObject exitButton;
    [SerializeField] private GameObject scrollView;
    [SerializeField] private GameObject mayaScene;

    private void Start()
    {
        aboutButton.SetActive(true);
        exitButton.SetActive(false);
        scrollView.SetActive(false);
        mayaScene.SetActive(true);
    }

    public void About()
    {
        aboutButton.SetActive(false);
        exitButton.SetActive(true);
        scrollView.SetActive(true);
        mayaScene.SetActive(false);
    }

    public void Exit()
    {
        aboutButton.SetActive(true);
        exitButton.SetActive(false);
        scrollView.SetActive(false);
        mayaScene.SetActive(true);
    }
}
