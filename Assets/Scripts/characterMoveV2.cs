﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterMoveV2 : MonoBehaviour
{

    [SerializeField] private Transform[] path;
    [SerializeField] private float minDist;
    [SerializeField] private float speed;

    private Vector3 distance;
    private int count = 0;

    // Update is called once per frame
    void Update()
    {
        //transform.position = Vector3.MoveTowards(transform.position, moveSpots[arrayPosition].position, speed*Time.deltaTime);
        transform.position += transform.forward * Time.deltaTime * speed;

        Quaternion lookDirection = Quaternion.LookRotation(path[count].position - transform.position);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, lookDirection, 45 * Time.deltaTime);

        if (Vector3.Distance(transform.position, path[count].position) < minDist)
        {
            count++;
            if(count == path.Length)
            {
                count = 0;
            }
        }
    }
}
