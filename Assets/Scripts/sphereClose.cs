﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sphereClose : MonoBehaviour
{
    [SerializeField] private audioVisuals audioScript;

    private void OnMouseDown()
    {
        audioScript.sphereClosed = true;
    }
}
